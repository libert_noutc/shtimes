from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import *
from django import forms

# Create your models here.


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name',
                  'email', 'password1', 'password2']


class ClasseForm (forms.ModelForm):
    class Meta:
        model = Classe
        fields = [ 'filiere', 'niveau', 'effectif']


class SpecialiterForm (forms.ModelForm):
    class Meta:
        model = Specialiter
        fields = ['code_classe' , 'nom', 'code_specialite', 'effectif', ]


class SalleForm (forms.ModelForm):
    class Meta:
        model = Salle
        fields = ['code_salle', 'capacite']


class EnseignantForm (forms.ModelForm):
    
    class Meta:
        model = Enseignant
        fields = ['matricule', 'nom', 'sexe']


class CoursesForm (forms.ModelForm):
    class Meta:
        model = Cours
        fields = ['code', 'nom', 'classe_id']


class DispenserForm (forms.ModelForm):
    class Meta:
        model = Dispenser
        fields = '__all__'


class ProgrammerForm (forms.ModelForm):
    class Meta:
        model = Programmer
        fields = '__all__'
