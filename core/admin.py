from django.contrib import admin

from core.models import *

# Register your models here.
admin.site.register(Specialiter)
admin.site.register(Salle)
admin.site.register(Cours)
admin.site.register(Programmer)
admin.site.register(Dispenser)
admin.site.register(Classe)
admin.site.register(Enseignant)
admin.site.register(Filiere)
admin.site.register(Etudiant)
