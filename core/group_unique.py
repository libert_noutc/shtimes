import string 
from django.utils.text import slugify 
import random 

def random_string_generator(size = 1, chars = ['A' 'B','C','D', 'E', 'F', 'G','H','I','G','J','K', 'L','M', 'N']): 
    return random.choice(chars) 
  
def unique_slug_generator(): 
    
    return random_string_generator()

       