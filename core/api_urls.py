from django.urls import path, include
from .views import *
from rest_framework.routers import DefaultRouter


# Wire up our API using automatic URL routing.
# Additionally, we include programmer URLs for the browsable API.

router = DefaultRouter()


router.register('profils', Programmerviewset, basename='profils')

urlpatterns = [
   path('v1/', include(router.urls)),
   path('', HomeView.as_view(), name='home' ),
   path('logout/', logout, name='logout' ),
   path('login/', login, name='login' ),
   path('timetable/', AutoTimeTable.as_view(), name='timetable' ),
   path('add-course/', CourseView, name='add-course'),
   path('update-course/<str:pk>/', updateCourseView, name='update-course'),
   path('delete-course/<str:pk>/', deleteCourse, name='delete-course'),

   path('course_view/', CourseTable, name='course_view'),
   path('professor_view/',  ListprofView.as_view(), name='professor_view'),
   path('delete-professor/<str:pk>/', deleteProfessor, name='delete-professor'),
   path('add-professor/', ProfessorView, name='add-professor'),
   path('update-professor/<str:pk>/', updateProfessorView, name='update-professor'),
   path('add-classroom/', ClassroomView, name='add-classroom'),
   path('update-classroom/<str:pk>/', updateClassroomView, name='update-classroom'),
   path('classroom_view/', ClassroomTable, name='classroom-view'),
   path('delete-classroom/<str:pk>/', deleteClassroom, name='delete-classroom'),
   path('class_view/', ClassTable, name='class_view'),
   path('delete-class/<str:pk>/', deleteClass, name='delete-class'),
   path('update-class/<str:pk>/', updateClassView, name='update-classview'),
   path('add-classcourse/', ClassCourseView, name='add-classcourse'),
   path('specialiter_view/', SpecialiterTable, name='specialiter_view'),
   path('delete-specialiter/<str:pk>/', deleteSpecialiter, name='delete-specialiter'),
   path('update-specialiter/<str:pk>/', updateSpecialiterView, name='update-specialiter'),
   path('add-specialiter/', SpecialterCourseView, name='add-specialiter'),
]


"""
   path('update-course/<str:pk>/', updateCourseView, name='update-course'),
   path('update-professor/<str:pk>/', updateProfessorView, name='update-professor'),
   path('update-class/<str:pk>/', updateClassView, name='update-classview'),
   path('delete-course/<str:pk>/', deleteCourse, name='delete-course'),
   path('course_view/', CourseTable, name='course_view'),
   path('professor_view/', ProfessorTable, name='professor_view'),
   path('delete-professor/<str:pk>/', deleteProfessor, name='delete-professor'),
   path('classroom_view/', ClassroomTable, name='classroom-view'),
   path('delete-classroom/<str:pk>/', deleteClassroom, name='delete-classroom'),
   path('class-view/', ClassTable, name='class_view'),
   path('delete-class/<str:pk>/', deleteClass, name='delete-class'),
"""