from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save

from .group_unique import unique_slug_generator
from django.db import connection

from .constant import *

class Filiere (models.Model):
    
    code_filier = models.CharField(max_length=255)
    nom_faculter = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    
    
    def __str__(self) -> str:
        return f"{self.nom}  "

    class Meta:
        unique_together = (('code_filier', 'nom_faculter'),)

    def create(self):
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO core_Filiere  VALUES (%s, %s, %s) ", [self.code_filier, self.nom_faculter, self.nom])
            row = cursor.fetchone()

        return row

    def all(self):
        pass
    
    def filter(self, *args, **kwargs) :
        pass
    
    def exclude(self, *args, **kwargs) :
        pass
    
    def union(self, *other_qs, all: bool ): 
        pass
    
    def intersection(self, *other_qs): 
        pass
    
    def difference(self, *other_qs):
        pass

class Salle (models.Model):
    code_salle = models.CharField(max_length=200)
    capacite =  models.IntegerField(default=500)

    def __str__(self) -> str:
        return f"{self.code_salle} {self.capacite} "


class Enseignant (models.Model):
  
    matricule = models.CharField(max_length=200)
    nom = models.CharField(max_length=200)
    sexe =models.CharField(max_length=200, choices=GENRE_SEXE)

    def __str__(self) -> str:
        return f"{self.matricule} {self.nom} {self.sexe}"


class Classe (models.Model):
    
    filiere = models.ForeignKey(Filiere, on_delete=models.CASCADE)
    niveau = models.CharField(max_length=255, choices=NIVEAU )
    effectif = models.IntegerField()
    groupe = models.CharField(max_length=20, default=' ', blank=True, null=True)

    def __str__(self) -> str:
        return f" {self.filiere} {self.niveau}  {self.groupe} "

    class Meta:
        unique_together = (('filiere', 'niveau', 'groupe'),)


def pre_save_receiver(sender, instance, *args, **kwargs):
    print('hello  ii world debug 1')
    qs_salle = Salle.objects.all().order_by('capacite')
    if instance.effectif > qs_salle[len(qs_salle)-1].capacite:
        loop_val = instance.effectif / qs_salle[len(qs_salle)-1].capacite 
        new_capaciter = instance.effectif / loop_val
        instance.effectif = new_capaciter
        instance.groupe = ' '
        klass = instance.__class__
        # Salle.__class__
        loop_val -= 1
        while (loop_val>0):
            print('hello ')
            val = unique_slug_generator()
            qs_exist =  klass.objects.filter(groupe=val).exists()
            if not qs_exist:
                klass.objects.create(filiere=instance.filiere, effectif=new_capaciter, niveau=instance.niveau, groupe=val)
                loop_val -=1

pre_save.connect(pre_save_receiver, sender = Classe) 


class Specialiter(models.Model):
    nom  = models.CharField(max_length=255)
    effectif = models.IntegerField()
    code_specialite = models.CharField(max_length=255)
    code_classe = models.ForeignKey(Classe, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.code_specialite}  {self.code_classe}  {self.effectif} "
    class Meta:
        unique_together = (('nom', 'code_specialite', 'code_classe',),)



class Courspecialiter (models.Model):
    

    code = models.CharField(max_length=200, unique=True)
    nom = models.CharField(max_length=200)
    clas_spec_code = models.ForeignKey(Specialiter, on_delete=models.SET_NULL, null=True, blank=True)
   
    def __str__(self) -> str:
        return f"{self.code} {self.clas_spec_code}"


class Cours (models.Model):
    

    code = models.CharField(max_length=200, unique=True)
    nom = models.CharField(max_length=200)
    classe_id = models.ForeignKey(Classe, on_delete=models.CASCADE)
   
    def __str__(self) -> str:
        return f"{self.code} {self.classe_id}"

    


class Dispenser (models.Model):
    code_prof = models.ForeignKey(Enseignant, on_delete=models.CASCADE)
    code_cours = models.ForeignKey(Cours, on_delete=models.CASCADE)
    def __str__(self) -> str:
        return f"{self.code} dispenser par {self.Matricule}"

class Programmer (models.Model):
   
    dispense = models.OneToOneField(Dispenser, on_delete=models.CASCADE)
    cours_type = models.CharField(max_length=200, choices=COURSE_TYPE)
    cours =models.ForeignKey(Cours, on_delete=models.CASCADE)
    salle = models.ForeignKey(Salle, on_delete=models.CASCADE)

    jour = models.CharField(max_length=200, choices=WEEK_DAY)
    heure_debut = models.TimeField()
    heure_fin = models.TimeField()

    def __str__(self) -> str:
        return f"ue: {self.cours}  salle: {self.salle}  days: {self.jour} hour: {self.heure_debut}:{self.heure_fin}"

    class Meta:
        unique_together = (('jour', 'heure_debut', 'heure_fin', 'cours', 'salle'),)

    



class Etudiant(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    classe = models.ForeignKey(Classe, on_delete=models.CASCADE)
    Specialiter = models.ForeignKey(Specialiter, on_delete=models.SET_NULL, null=True, blank=True)
    