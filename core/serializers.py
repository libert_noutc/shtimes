from venv import create
from django.db import models
from rest_framework.serializers import Serializer, ModelSerializer
from rest_framework import fields, serializers
from .models import *
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import User


class FilierSerializer(ModelSerializer):
    class Meta:
        model = Filiere
        fields = '__all__'


class Login1Serializer(Serializer):
    """
    Description: Model Description
    """
    username = fields.CharField()
    password = fields.CharField(write_only=True, required=True)
    token = fields.SerializerMethodField()

    def get_token(self, instance: User):
        return Token.objects.get(user=instance).key



class ClasseSerializer(ModelSerializer):
    """
    Description: Model Description
    """
    filiere = FilierSerializer()

    class Meta:
        model = Classe
        fields = ('id', 'groupe', 'filiere',
                   'niveau', 'effectif')


    def create(self, validated_data):
        filiere = validated_data.pop('filiere')
        clas = Classe.objects.create(**validated_data)
        Filiere.objects.create(**clas)
        

        return clas

    def update(self, instance:Classe, validated_data):
       

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance


class SpecialiterSerializer(ModelSerializer):
    """
    Description: Model Description
    """
    
    # code_specialite =  ClasseSerializer()
    code_classe =  ClasseSerializer()
    class Meta:
        model = Specialiter
        fields = ('id', 'code_classe', 'nom','code_specialite')


    def create(self, validated_data):
        code_classe = validated_data.pop('code_classe')
        code_classe = Classe.objects.create(**code_classe)
        classe_speciale = Specialiter.objects.create(code_classe = code_classe, **validated_data)
        
        
        return classe_speciale

    def update(self, instance:Specialiter, validated_data):
        code_classe = validated_data.pop('code_classe')

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance


class SalleSerializer(ModelSerializer):
    """
    Description: Model Description
    """
    

    class Meta:
        model = Salle
        fields = ('id', 'code_salle', 'capacite',)


    def create(self, validated_data):
        
        return Salle.objects.create(**validated_data)

    def update(self, instance:Salle, validated_data):
       

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance


class EnseignantSerializer(ModelSerializer):
    """
    Description: Model Description
    """
    

    class Meta:
        model = Enseignant
        fields = ('id', 'sexe', 'nom','matricule')


    def create(self, validated_data):
        
        return Enseignant.objects.create(**validated_data)

    def update(self, instance:Enseignant, validated_data):
       

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance


class CoursSerializer(ModelSerializer):
    """
    Description: Model Description
    """
    
    classe_id =  ClasseSerializer()

    class Meta:
        model = Cours
        fields = ('id', 'code', 'classe_id', 'nom',)


    def create(self, validated_data):
        classe_id = validated_data.pop('classe_id')
        cours = Cours.objects.create(**validated_data)
        return Cours.objects.create(**validated_data)

    def update(self, instance:Cours, validated_data):
       

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance



class DispenserSerializer(ModelSerializer): 
    """
    Description: Model Description
    """
    
    code_prof = EnseignantSerializer()
    code = CoursSerializer()

    class Meta:
        model = Dispenser
        fields = ['code_prof', 'code']


    def create(self, validated_data):
        prof = validated_data.pop('code_prof')
        code = validated_data.pop('code')
        return Dispenser.objects.create(code_prof=prof, code=code)

    def update(self, instance:Dispenser, validated_data):
       

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance




class ProgrammerSerializer(ModelSerializer): 
    """
    Description: Model Description
    """
    
    dispensers = DispenserSerializer()
    salle = SalleSerializer()
    cours = CoursSerializer()

    class Meta:
        model = Programmer
        fields = ['id', 'cours', 'jour', 'heure_debut', 'heure_fin', 'salle', 'dispensers']


    def create(self, validated_data):
        dispensers_data = validated_data.pop('dispensers')
        salle_data = validated_data.pop('salle')
        cours_data = validated_data.pop('cours')
        dispensers_data = Dispenser.objects.create(**dispensers_data)
        salle_data = Salle.objects.create(**salle_data)
        cours_data = Cours.objects.create(**cours_data)
        programme = Programmer.objects.create(dispensers_data=dispensers_data, salle_data=salle_data, cours_data=cours_data, **validated_data)


        return programme

    def update(self, instance:Programmer, validated_data):
       

        for key, value in validated_data.items():

            setattr(instance, key, value)
            
            instance.save()

        return instance


class tableSerializers(ModelSerializer):


    class Meta:
        model = Programmer 
        fields = ('id', 'cours', 'code', 'jour', 'heure_debut', 'heure_fin', 'salle', 'Matricule')

