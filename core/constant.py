
## level of student 
LICENSE = 'LICENSE'
LICENSE1 = 'LICENSE 1'
LICENSE2 = 'LICENSE 2'
LICENSE3 = 'LICENSE 3'

MASTER = 'MASTER'
MASTER1 = 'MASTER 1'
MASTER2 = 'MASTER 2'

DOCTORAT =  'DOCTORAT'
DOCTORAT1 =  'DOCTORAT 1'
DOCTORAT2 =  'DOCTORAT 2'
DOCTORAT3 = 'DOCTORAT 3'
NIVEAU = (

    (LICENSE , (
        (LICENSE1 , 'LICENSE 1'),
        (LICENSE2 , 'LICENSE 2'),
        (LICENSE3 ,'LICENSE 3')
        )
    ),
    (MASTER ,(
        (MASTER1 , 'MASTER 1'),
        (MASTER2 , 'MASTER 2'),
        
        )),
    (DOCTORAT , (
        (DOCTORAT1 , 'DOCTORAT 1'),
        (DOCTORAT2 , 'DOCTORAT 2'),
        (DOCTORAT3 ,'DOCTORAT 3')
        )),
    
)

## SPECIALITER 
GENIE_LOGICIEL_ET_SYSTEME_D_INFORMATION = 'GENIE LOGICIEL ET SYSTEME D\'INFORMATION'
RESEAU_ET_SYSTEME = 'RESEAU ET SYSTEME' 
DATA_SCIENCE = 'DATA SCIENCE'
SECURITER = 'SECURITER' 

SPECIALITER = (
    (GENIE_LOGICIEL_ET_SYSTEME_D_INFORMATION , 'GENIE LOGICIEL ET SYSTEME D\'INFORMATION'),
    (RESEAU_ET_SYSTEME , 'RESEAU ET SYSTEME' ),
    (DATA_SCIENCE , 'DATA SCIENCE'),
    (SECURITER , 'SECURITER'),
)

HOMME = 'HOMME'
FEMME = 'FEMME'

GENRE_SEXE = (
    (HOMME ,  'HOMME'),
    (FEMME , 'FEMME'),
)
WEEK_DAY_Liste = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
# Create your models here.
WEEK_DAY = (
    ('Monday', 'Monday'),
    ('Tuesday', 'Tuesday'),
    ('Wednesday', 'Wednesday'),
    ('Thursday', 'Thursday'),
    ('Friday', 'Friday'),
    ('Saturday', 'Saturday'),
    ('Sunday', 'Sunday')
    )

COURSE_TYPE = (
    ('Theory', 'Theory'),
    ('TD', 'TD')
)