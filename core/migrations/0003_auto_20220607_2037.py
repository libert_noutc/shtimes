# Generated by Django 3.2.13 on 2022-06-07 20:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_rename_matricule_dispenser_code_prof'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cours',
            name='clas_spec_code',
        ),
        migrations.CreateModel(
            name='Courspecialiter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=200, unique=True)),
                ('nom', models.CharField(max_length=200)),
                ('clas_spec_code', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.specialiter')),
            ],
        ),
    ]
