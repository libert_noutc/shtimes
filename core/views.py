import json
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from .forms import *
from .models import *
from django.db.models import F
# Create your views here.
from django.views.generic import View
import random
from datetime import datetime
from rest_framework.permissions import IsAuthenticated
from rest_framework import routers, serializers, viewsets, views, status
from rest_framework.mixins import (CreateModelMixin, DestroyModelMixin,
                                   ListModelMixin, UpdateModelMixin, RetrieveModelMixin)
from rest_framework.viewsets import GenericViewSet
from rest_framework.views import APIView
from .serializers import *
from django.contrib.auth import logout, login, authenticate
from rest_framework.decorators import action
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import RetrieveAPIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from .models import *
from rest_framework.permissions import BasePermission, IsAuthenticated, AllowAny, SAFE_METHODS


class Programmerviewset(CreateModelMixin, DestroyModelMixin,
                                   ListModelMixin, UpdateModelMixin,GenericViewSet):
    queryset = Programmer.objects.all()
    serializer_class = ProgrammerSerializer
    permission_classes = [AllowAny, ]

    @swagger_auto_schema(
        
        operation_description="get the liste of evry   programmate cours ")
    @action(methods=["get"], detail=False)
    def signin(self, request, *args, **kwargs):


        class_programmete = ProgrammerSerializer(class_programmete)
        programme = DispenserSerializer(programme)
        res = tableSerializers((class_programmete,programme))
        # print(res.data)
                
        print(programme, class_programmete)
         
        return Response(res, status=status.HTTP_200_OK)

class Coursprofviewset(ListModelMixin, GenericViewSet):
    queryset = Programmer.objects.all()
    serializer_class = ProgrammerSerializer
    permission_classes = [AllowAny, ]

    @swagger_auto_schema(
        
        operation_description="get the liste of evry   programmate cours of one prof ")
    @action(methods=["get"], detail=False)
    def get_one_prof(self, request, pk, *args, **kwargs):

        Prof = Enseignant.objects.get(pk=pk)
        class_programmete = Programmer.objects.all().filter(dispensers__code_prof=Prof)
        class_programmete = ProgrammerSerializer(class_programmete, many=True)
       
        return Response(class_programmete.data, status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        
        operation_description="get the liste of evry   programmate cours of one salle ")
    @action(methods=["get"], detail=False)
    def get_one_salle(self, request, pk, *args, **kwargs):

        Prof = Salle.objects.get(pk=pk)
        class_programmete = Programmer.objects.all().filter(salle=Prof)
        class_programmete = ProgrammerSerializer(class_programmete, many=True)
       
        return Response(class_programmete.data, status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        
        operation_description="get the liste of evry   programmate cours of one salle ")
    @action(methods=["get"], detail=False)
    def get_one_all_cours_for_student(self, request, pk, *args, **kwargs):
        student = Etudiant.objects.get(pk=pk)
        Prof = Classe.objects.get(pk=student.id)
        specialiter = Specialiter.objects.get(code_classe=Prof)
        class_programmete = Programmer.objects.all().filter(cours__classe_id=Prof)
        class_programmete = ProgrammerSerializer(class_programmete, many=True)
       
        return Response(class_programmete.data, status=status.HTTP_200_OK)


class HomeView(View):
    template_name = 'core/home.html'
    def get(self, request, *args, **kwargs):
        classe = Classe.objects.get(pk=1)
        list_specia = Cours.objects.create(code ='info32', nom ='helios',  classe_id =classe)
        list_query = classe.query
        return render(request, self.template_name, context={'query':list_query})
    
def loginPage(request):
    context = {}
    if request.user.is_authenticated:
        return redirect('/')
    else:
        
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password= password )
            
            if user is not None:
                login(request, user)
                return redirect('selection')
            else:
                messages.info(request, 'Username or Password is Incorrect')
                return render(request, 'core/login.html', context)

        
        return render(request, 'core/login.html', context)

class Home (View):
    template_name = 'core'
    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, 'core/main.html', context)

def Logout(request):
    logout(request)
    return redirect('login')


class AutoTimeTable(View):
    template_name = 'core/listclass.html'

    def post(self, request,   *args, **kwargs):
        data = json.loads(request.body)
        dates = {

            1: ['7:00:00', '9:55:00'],
            2: ['10:00:00', '12:55:00'],
            3: ['13:00:00', '15:55:00'],
            4: ['16:00:00', '18:55:00'],
            5: ['19:00:00', '20:55:00'],

        }
        pk = data['Id']
        classe = get_object_or_404(Classe, pk=pk)
        Specialiter_classe = Specialiter.objects.filter(code_classe=classe.id) 
        cours = Cours.objects.filter(classe_id=pk)
        print(cours)
        salles = Salle.objects.all()
        clas = classe
        salle_i =salles.filter(capacite__gte=clas.effectif).order_by(F('capacite').asc())
        print('liste de salle :', salle_i, salle_i.first)
        salle_1 = salle_i[0]
        for cour in cours:
            # cours_type
            loop =True
            print('jour')
            while loop:
                cour = cour
                amphi = salle_1
                jour = random.choice(WEEK_DAY)
                print(jour)
                plage_horaire = dates[random.randint(1,5)] 
                heure_debut = plage_horaire[0]
                heure_fin =  plage_horaire[1]
                if not Programmer.objects.filter(cours=cour, jour=jour, salle=amphi , heure_debut=heure_debut, heure_fin=heure_fin).exists():
                    loop = False
                    prog = Programmer.objects.create(cours=cour, jour=jour, salle=amphi,  heure_debut=heure_debut, heure_fin=heure_fin)
                    prog.jour = jour[0]
                    prog.save()
                    print(prog)
                else:
                    print('un cours programmer')  
            
        if Specialiter_classe.exists():
            classe_specialiter_code = Cours.objects.filter(clas_spec_code=Specialiter_classe)
        
        

        time_slot = [ '7:00:00 - 9:55:00', '10:00:00 - 12:55:00', '13:00:00 - 15:55:00', '16:00:00 - 18:55:00', '19:00:00 - 20:55:00']
        
        print(time_slot)
        context = {
            'time_slot':time_slot,
           # 'sections': section    
        }
        return JsonResponse('contact was added', safe=False)


def auto_create(ue, salle, jour, heure_debut, heure_fin, i=1):

    programmer = Programmer
    if  programmer.objects.filter(cours=ue.id,  salle=salle[0],
                                                        jour=jour,
                                                        heure_debut=heure_debut,
                                                        heure_fin=heure_fin).exists():
        return False
    else:
        if i == 1:

            programmer.objects.create(cours=ue, salle=salle[0],
                                jour=jour,
                                heure_debut=heure_debut,
                                heure_fin=heure_fin)
        else:
            print('hello add grous')

            nom = ue.code_classe.code_classe 
            if not 'GROUPE_' in nom:
                ue.code_classe.code_classe += '  GROUPE_' + str(i)
            else:
                nom = nom[0:-2] + str(i)
                ue.code_classe.code_classe += nom

            ue.code_classe.save()
            ue.save()
            programmer.objects.create(cours=ue, salle=salle[0],
                                jour=jour,
                                heure_debut=heure_debut,
                                heure_fin=heure_fin)

        return True


def ProfessorView(request):
    professor = EnseignantForm()
    professor1 = Enseignant.objects.all()

    context = {'professor': professor, 'professor1': professor1}
    if request.method == 'POST':
        professor = EnseignantForm(request.POST)
        if professor.is_valid():
            messages.success(request, 'Professor has been added successfully.')
            professor.save()
            return redirect('core:professor_view')
        else:
            messages.success(request, 'Professor already exists or you have added wrong attributes.')
    return render(request, 'core/addprofessor.html', context)


def ProfessorTable(request):
    professor1 = Enseignant.objects.all()
    context = {'professor1': professor1}
    return render(request, 'core/professortable.html', context)

def updateProfessorView(request, pk):
    professor = get_object_or_404(Enseignant, pk=pk) 
    form = EnseignantForm(instance=professor)
    context = {'form': form}
    if request.method == 'POST':
        form = EnseignantForm(request.POST, instance=professor)
        if form.is_valid():
            form.save()
            return redirect('core:professor_view')
    return render(request, 'core/section.html', context)

class ListprofView(View):

    def get(self, request, *args, **kwargs):

        professor1 = Enseignant.objects.all()
        context = {'professor1': professor1}
        return render(request, 'core/professortable.html', context)


def deleteProfessor(request, pk, *args, **kwargs):
    deleteprofessor = get_object_or_404(Enseignant, pk=pk) 
    context = {'delete': deleteprofessor}
    if request.method == 'POST':
        deleteprofessor.delete()
        return redirect('/professor_view')

    return render(request, 'core/deleteprofessor.html', context)


def CourseTable(request):

    course = Cours.objects.all()
    context = {'course': course}
    
    return render(request, 'core/courselist.html', context)

def CourseView(request):
    course = CoursesForm()
    context = {'course': course}

    if request.method == 'POST':
        course = CoursesForm(request.POST)
        if course.is_valid():
            messages.success(request, 'Course has been added successfully.')
            course.save()
            return redirect('core:course_view')
        else:
            messages.success(request, 'Course already exists or you have added wrong attributes.')


    return render(request, 'core/addcourse.html', context)


def updateCourseView(request, pk):
    form = get_object_or_404(Cours, pk=pk) 
    course = CoursesForm(instance=form)
    context = {'course': course}
    if request.method == 'POST':
        course = CoursesForm(request.POST, instance=form)
        if course.is_valid():
            course.save()
            return redirect('core:course_view')
    return render(request, 'core/addcourse.html', context)



def deleteCourse(request, pk):
    delete_course = get_object_or_404(Cours, pk=pk) 
    context = {'course_delete': delete_course}
    if request.method == 'POST':
        delete_course.delete()
        return redirect('core:course_view')

    return render(request, 'core/delete.html', context)





def ClassroomView(request):
    classroom = SalleForm()
    classes = Salle.objects.all()
    context = {'classroom': classroom, 'classes': classes}
    if request.method == 'POST':
        classroom = SalleForm(request.POST)
        if classroom.is_valid():
            messages.success(request, 'Salle has been added.')
            classroom.save()
            return redirect('/classroom_view')
        else:
            messages.error(request, 'Do not enter the same class ID')
    return render(request, 'core/createclassroom.html', context)

def ClassroomTable(request):
    classrooms = Salle.objects.all()
    context = {'classrooms': classrooms}
    return render(request, 'core/listeclassroom.html', context)


def updateClassroomView(request, pk):
    classroom = get_object_or_404(Salle, pk=pk)
    form = SalleForm(instance=classroom)
    context = {'form': form}
    if request.method == 'POST':
        form = SalleForm(request.POST, instance=classroom)
        if form.is_valid():
            form.save()
            return redirect('/classroom_view')
    return render(request, 'core/section.html', context)


def deleteClassroom(request, pk):
    deleteClassroom = get_object_or_404(Salle, pk=pk)
    context = {'delete': deleteClassroom}
    if request.method == 'POST':
        deleteClassroom.delete()
        return redirect('/classroom_view')

    return render(request, 'core/deletesalle.html', context)



def ClassCourseView(request):
    sectioncourse = ClasseForm()
    sectioncourses = Classe.objects.all()
    
    context = {'sectioncourse': sectioncourse, 'sectioncourses': sectioncourses}
    if request.method == 'POST':
        sectioncourse = ClasseForm(request.POST)
        if sectioncourse.is_valid():
            messages.success(request, "Course added for class.")
            sectioncourse.save()
            return redirect('/class-view')
        else:
            messages.error(request, 'Can not add duplicate course for class.')
    return render(request, 'core/addclass.html', context)


def ClassTable(request):
    sections = Classe.objects.all()
    context = {'sections': sections}
    return render(request, 'core/listclass.html', context)



def updateClassView(request, pk):
    section = get_object_or_404(Classe, id=pk)
    form = ClasseForm(instance=section)
    context = {'form': form}
    if request.method == 'POST':
        form = ClasseForm(request.POST, instance=section)
        if form.is_valid():
            form.save()
            return redirect('/class-view')
    return render(request, 'core/ViewClass.html', context)


def deleteClass(request, pk):
    deleteClass = get_object_or_404(Classe, id=pk)
    context = {'delete': deleteClass}
    if request.method == 'POST':
        
        deleteClass.delete()

        return redirect('/class-view')

    return render(request, 'core/deleteClass.html', context)




def SpecialterCourseView(request):
    specialiteradd = SpecialiterForm()
    sectioncourses = Specialiter.objects.all()
    
    context = {'specialiteradd': specialiteradd, 'sectioncourses': sectioncourses}
    if request.method == 'POST':
        specialiteradd = SpecialiterForm(request.POST)
        if specialiteradd.is_valid():
            messages.success(request, "Course added for class.")
            specialiteradd.save()
            return redirect('/specialiter_view')
        else:
            messages.error(request, 'Can not add duplicate course for class.')
    return render(request, 'core/specialiteradd.html', context)


def SpecialiterTable(request):
    sections = Specialiter.objects.all()
    context = {'sections': sections}
    return render(request, 'core/specialiterlist.html', context)




def updateSpecialiterView(request, pk):
    section = get_object_or_404(Specialiter, id=pk)
    form = SpecialiterForm(instance=section)
    context = {'form': form}
    if request.method == 'POST':
        form = SpecialiterForm(request.POST, instance=section)
        if form.is_valid():
            form.save()
            return redirect('/specialiter_view')
    return render(request, 'core/updatespecialiter.html', context)


def deleteSpecialiter(request, pk):
    deleteClass = get_object_or_404(Specialiter, id=pk)
    context = {'delete': deleteClass}
    if request.method == 'POST':
        
        deleteClass.delete()

        return redirect('/specialiter_view')

    return render(request, 'core/specialiterlist.html', context)
